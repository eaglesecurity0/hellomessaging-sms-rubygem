require "helliomessaging_sms/version"
require 'cgi'
require 'curb-fu'
require 'nokogiri'

module HellioMessaging
  @@api_username = nil
  @@api_password = nil
  @@api_comsumer_key = nil
  @@api_application_secret = nil
  @@api_senderid = "HELLIOAPI"

  # Expects :msg, :to and an optional :from param
  # The :from param defaults to @@api_senderid when its omitted
  def self.push(options={})

    sender_id = options[:from].nil? ? @@api_senderid : options[:from]
    response = nil
    message = options[:msg]

    raise ArgumentError, ':msg and :to params expected' if message.nil? || options[:to].nil?

    message = Nokogiri::HTML.parse(message).text

    # Send SMS Using the old form of Authentication [Username and Password]
    if @@api_username != nil && @@api_password != nil
      response = CurbFu.get({:host => 'app.helliomessaging.com/api', :path => '/sms?'}, { :from => sender_id, :to => options[:to], :text => message, :username => @@api_username, :password => @@api_password })
    end


    # Send SMS Using the new form of Authentication [Consumer ID and an Application Serect]
    if @@api_consumer_key != nil && @@api_application_secret != nil
      response = CurbFu.get({:host => 'app.helliomessaging.com/api', :path => '/sms?', :protocol => 'http'}, { :From => sender_id, :To => options[:to], :Content => message, :ConsumerKey => @@api_consumer_key, :ApplicationSecret => @@api_application_secret })
    end

     # Check Your Account Balance (Old Authentication)
    if @@api_username != nil && @@api_password != nil
      response = CurbFu.get({:host => 'app.helliomessaging.com/api', :path => '/sms-bal?', :protocol => 'http'}, { :username => @@api_username, :password => @@api_password })
    end

    # Check Your Account Balance (New Authentication: Consumer Key and Application Secret)
    if @@api_consumer_key != nil && @@api_application_secret != nil
      response = CurbFu.get({:host => 'app.helliomessaging.com/api', :path => '/sms-bal?', :protocol => 'http'}, { :ConsumerKey => @@api_consumer_key, :ApplicationSecret => @@api_application_secret })
    end


     # Check for DLR's [Consumer ID and an Application Serect or Username and Password]
    if @@api_consumer_key != nil && @@api_application_secret != nil
      response = CurbFu.get({:host => 'app.helliomessaging.com/api', :path => '/sms-dlr?', :protocol => 'http'}, { :From => sender_id, :To => options[:to], :Content => message, :ConsumerKey => @@api_consumer_key, :ApplicationSecret => @@api_application_secret })
    end

    {:status => response.status, :notice => response.body}

  end


  def self.api_username=(api_username); @@api_username = api_username; end
  def self.api_username; @@api_username; end

  def self.api_password=(api_password); @@api_password = api_password; end
  def self.api_password; @@api_password; end

  def self.api_consumer_key=(api_consumer_key); @@api_consumer_key = api_consumer_key; end
  def self.api_consumer_key; @@api_consumer_key; end

  def self.api_application_secret=(api_application_secret); @@api_application_secret = api_application_secret; end
  def self.api_application_secret; @@api_application_secret; end

  def self.api_senderid=(api_senderid); @@api_senderid = api_senderid; end
  def self.api_senderid; @@api_senderid; end

end
