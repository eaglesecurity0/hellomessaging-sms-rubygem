# -*- encoding: utf-8 -*-
require File.expand_path('../lib/helliomessaging_sms/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Albert A. Ninyeh"]
  gem.email         = ["albert@sarayu.co, eaglesecurity0@gmail.com"]
  gem.description   = %q{Helliomessaging is a Ruby interface to the Hellio Messaging's Bulk SMS Gateway. It can be used to easily integrate SMS features into your application. It supports sending messages, checking balance and recieving delivery reports. You need to have a valid Hellio Messaging account to use this gem. You can get one at https://helliomessaging.com.}
  gem.summary       = %q{Send SMS via Hellio Messaging's API}
  gem.homepage      = "https://gitlab.com/eaglesecurity0/hellomessaging-sms-rubygem"

  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.name          = "helliomessaging_sms"
  gem.require_paths = ["lib"]
  gem.version       = HellioMessaging::VERSION
  gem.licenses      = ['MIT']
  #gem.add_dependency "curb-fu", "~> 0.6.2"
  gem.add_dependency "curb-fu", "~> 0"
  gem.add_runtime_dependency 'nokogiri', '~> 0'
  #gem.add_dependency "nokogiri"
end
