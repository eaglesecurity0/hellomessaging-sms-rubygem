# helliomessaging_sms

This is a ruby wrapper that abstracts Hellio Messaging SMS API. This gem has extra features and simplicity which makes it seamlessly easy to Send Bulk SMS, Check Balance and Receive delivery report.

## Installation

Add this line to your application's Gemfile:

    gem 'helliomessaging_sms'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install helliomessaging_sms

## Usage

Create an Account with Hellio Messaging via https://helliomessaging.com and head to your account settings to generate your API credentials which could be the old Username and Password sort of authentication or using a Consumer Key and an Application Serect.

If you are using the old API (V1) then

	helliomessaging_sms.api_username = "YOUR_USERNAME"
	helliomessaging_sms.api_password = "YOUR_PASSWORD"

If you are using the new API (V2) then

	helliomessaging_sms.api_consumer_id = "YOUR_CONSUMER_KEY"
	helliomessaging_sms.api_application_secret = "YOUR_APPLICATION_SECRET"
	
Optionally set global Sender ID

	helliomessaging_sms.api_senderid = "YOUR_SENDERID"
	
Sending an SMS Message

	helliomessaging_sms.push(:to => 'PHONE-NUMBER', :msg => "Your SMS Message goes here")
	
How to override the global SENDER ID config

	helliomessaging_sms.push(:to => 'PHONE-NUMBER', :msg => "Your SMS Message goes here", :from => "SENDERID")
	
Sample Results Returned

	{:status=>200, :notice=>"OK: 62357215"}
	
Any returned status code other than 200 is a failed response.

## About Hellio Messaging

Hellio Messaging is the premier messaging company in Ghana. Find out more about them at https://helliomessaging.com

## Credits
Special Thanks to Alfred Rowe for granting me the permission to fork this gem and also for everyone who helped in testing the first release.

Developed by Albert A. Ninyeh
Thanks to Alfred Rowe {https://github.com/nukturnal/smsgh_sms}

## License
MIT

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
